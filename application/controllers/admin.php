<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Admin extends CI_Controller {

  function __construct()
  {
    parent::__construct();
  }
public function customers(){
     $data['title'] = "Customers";
     $data['customers'] = $this->get_customers();
      $this->load->view('header_admin', $data);
      $this->load->view('customers', $data);
      $this->load->view('footer_admin', $data);
}
  public function add_customer(){
  	$data['title'] = "Add Customer";
    $this->load->view('header_admin', $data);
    $this->load->view('add_customer', $data);
    $this->load->view('footer_admin', $data);
  }
  public function add_user(){
  	$data['title'] = "Add User";
    $this->load->view('header_admin', $data);
    $this->load->view('add_user', $data);
    $this->load->view('footer_admin', $data);
  }
  public function process_add_customer(){
  	$this->db->query("insert into customers(c_name, c_account, c_phone, c_email, c_address, c_company, c_website, c_contactperson_title, c_pobox)values('$_POST[name]', '$_POST[account]', '$_POST[phone]', '$_POST[email]', '$_POST[address]', '$_POST[company]', '$_POST[website]', '$_POST[title]', '$_POST[pobox]')");
	
	//Add Type
	$data['title'] = "Add Customer";
	$data['result'] = "Added Successfully!";
    $this->load->view('header_admin', $data);
    $this->load->view('add_customer', $data);
    $this->load->view('footer_admin', $data);
  }
  public function process_add_user(){
  	$pass = md5($_POST['password']);
  	$this->db->query("insert into users(username, password, email, isadmin)values('$_POST[name]', '$pass', '$_POST[email]', '$_POST[isadmin]')");

	$data['title'] = "Add User";
	$data['result'] = "Added Successfully!";
    $this->load->view('header_admin', $data);
    $this->load->view('add_user', $data);
    $this->load->view('footer_admin', $data);
  }
  function index()
  {
    if($this->session->userdata('logged_in'))
    {
      $session_data = $this->session->userdata('logged_in');
      $data['username'] = $session_data['username'];
	  $data['title'] = "Admin";
	  $data['orders'] = $this->get_orders();
	  $data['stock'] = $this->get_stock();
      $this->load->view('header_admin', $data);
      $this->load->view('admin_view', $data);
      $this->load->view('footer_admin', $data);
    }
    else
    {
      //If no session, redirect to login page
      redirect('login', 'refresh');
	}
  }
	public function menus(){
		 $data['title'] = "Menus";
	  $data['menus'] = $this->get_stock();
     $this->load->view('header_admin', $data);
      $this->load->view('menus', $data);
     $this->load->view('footer_admin', $data);
	}
	public function calendar(){
		 $data['title'] = "Calendar";
     $this->load->view('header_admin', $data);
      $this->load->view('calendar', $data);
     $this->load->view('footer_admin', $data);
	}
	public function outsource(){
	$data['title'] = "Outsource";
	$data['type'] = $this->get_type_name($_GET['type']);
     $this->load->view('header_admin', $data);
      $this->load->view('outsource', $data);
     $this->load->view('footer_admin', $data);
	}
	public function process_outsource(){
  	$this->db->query("insert into outsource(out_company, out_item, out_total)values('$_POST[name]', '$_POST[type]', '$_POST[total]')");
	redirect(base_url()."index.php/admin");
  }
	public function add_menu_section($result=null){
	 $data['title'] = "Add Menu Section";
	 $data['result'] = $result;
     $this->load->view('header_admin', $data);
     $this->load->view('add_menu_section', $data);
     $this->load->view('footer_admin', $data);
	}
	public function process_menu_section(){
		$post = $_POST;
		$this->db->query("insert into menu_item(mi_name, mi_menu, mi_order)values('$post[name]', '$post[menu]', '$post[order]')");	
		$result = "Added Successully!";
		$this->add_menu_section($result);
	}
	public function add_menu_food($result=null){
		$data['title'] = "Add Menu Food";
		$data['menuitems'] = $this->get_menu_items($_GET['menu']);
		$this->load->view('header_admin', $data);
		$this->load->view('add_menu_food', $data);
		$this->load->view('footer_admin', $data);
	}
	public function process_menu_food($result=null){
		$menuitem = $_POST['menuitem'];
		$food = $_POST['name'];
		$order = $_POST['order'];
		$menu = $_POST['menu'];
		$data['title'] = "Add Menu Food";
		$this->db->query("insert into menu_food(mf_name, mf_mi, mf_order)values('$food', '$menuitem', '$order')");
		$result = "Food added successfully!";
		$data['result'] = $result;
		$data['menu'] = $menu;
		$data['menuitems'] = $this->get_menu_items($_POST['menu']);
		$this->load->view('header_admin', $data);
		$this->load->view('add_menu_food', $data);
		$this->load->view('footer_admin', $data);
	}
	public function get_menu_items($menu){
		$menuitems = $this->db->query("select * from menu_item where mi_menu='$menu'");
		$menuitems = $menuitems->result_array();
		return $menuitems;
	}
  public function stock(){
  	  $data['title'] = "Stock";
	  $data['stock'] = $this->get_stock();
     $this->load->view('header_admin', $data);
      $this->load->view('stock', $data);
     $this->load->view('footer_admin', $data);
  }
  public function invoice(){
	$data['vat'] = $this->get_option(4);
  	$data['order'] = $this->get_order($_GET['id']);
   	$this->load->view('invoice', $data);
  }
  public function reciept(){
   	$this->load->view('reciept', $data);
  }
  public function replacementinvoice(){
	$data['vat'] = $this->get_option(4);
  	$data['order'] = $this->get_order($_GET['id']);

   	$this->load->view('replacementinvoice', $data);
  }
public function gatepass(){
  	$data['order'] = $this->get_order($_GET['id']);
   	$this->load->view('gatepass', $data);
  }
  public function get_options(){
	$this->db->select("*");
	$this->db->from("options");
	$result = $this->db->get();
	return $result->result_array();
}
public function get_option($id){
	$this->db->select("*");
	$this->db->from("options");
	$this->db->where("opt_id", $id);
	$result = $this->db->get();
	$result = $result->result_array();
	return $result[0]['opt_value'];
}
public function update_options(){
	$post = $_POST;	
	foreach($post as $k=>$v){
		$this->db->query("update options set opt_value='$v' where opt_id='$k'");
	}
	$data['result'] = "Options Updated Successfully!";
	$data['options'] = $this->get_options();
	$data['title'] = "Settings";
	$this->load->view('header_admin', $data);
     	$this->load->view('options', $data);
     	$this->load->view('footer_admin', $data);
}
	public function options(){
		$data['options'] = $this->get_options();
		$data['title'] = "Settings";
		$this->load->view('header_admin', $data);
	     	$this->load->view('options', $data);
	     	$this->load->view('footer_admin', $data);
	}
  function logout()
  {
    $this->session->unset_userdata('logged_in');
    session_destroy();
    redirect('admin', 'refresh');
  }
  public function add_type(){
  	$data['title'] = "Admin";
    $this->load->view('header_admin', $data);
    $this->load->view('add_type', $data);
    $this->load->view('footer_admin', $data);
  }
public function customer(){
	$id = $_GET['id'];
	$data['invoices'] = $this->get_customer_invoices($id);
	$data['title'] = "Customer Details";
    $this->load->view('header_admin', $data);
    $this->load->view('customer', $data);
    $this->load->view('footer_admin', $data);
}
public function get_customer_invoices($id){
	$name = $this->get_customer_name($id);
		$this->db->select("*");
		$this->db->from("orders");
		$this->db->where("o_name", $name);
		$result = $this->db->get();
		$result = $result->result_array();
		return $result;
}
public function get_customer_name($id){
		$this->db->select("*");
		$this->db->from("customers");
		$this->db->where("c_id", $id);
		$result = $this->db->get();
		$result = $result->result_array();
		return $result[0]['c_name'];
}
public function add_purchases(){
  	$data['title'] = "Admin";
	 $data['stock'] = $this->get_stock();
    $this->load->view('header_admin', $data);
    $this->load->view('add_purchases', $data);
    $this->load->view('footer_admin', $data);
  }
public function purchases(){
  	$data['title'] = "Purchases";
	 $data['purchases'] = $this->get_purchases();
    $this->load->view('header_admin', $data);
    $this->load->view('purchases', $data);
    $this->load->view('footer_admin', $data);
  }
public function process_add_purchases(){
	$id = $_POST['name'];
	$this->db->select("*");
	$this->db->from("types");
	$this->db->where("t_id", $id);
	$result = $this->db->get();
	$result = $result->result_array();
	$total = $result[0]['t_total'];
	$total = $total + $_POST['amount'];
	$this->db->query("update types set t_total='$total' where t_id='$id'");
  	$this->db->query("insert into purchases(p_type, p_amount)values('$_POST[name]', '$_POST[amount]')");
	
	$data['result'] = "Added Successfully!";
	$data['title'] = "Admin";
	 $data['stock'] = $this->get_stock();
    $this->load->view('header_admin', $data);
    $this->load->view('add_purchases', $data);
    $this->load->view('footer_admin', $data);	
  }
  public function process_add_type(){
  	$this->db->query("insert into types(t_name, t_total, t_price, t_replacement_cost, t_purchase_price)values('$_POST[name]', '$_POST[amount]', '$_POST[price]', '$_POST[replacement]', '$_POST[purchase]')");
	
	//Add Type
	$data['title'] = "Admin";
	$data['result'] = "Added Successfully!";
    $this->load->view('header_admin', $data);
    $this->load->view('add_type', $data);
    $this->load->view('footer_admin', $data);
  }
  public function process_order_admin(){
		$this->db->query("insert into orders(o_name, o_delivery, o_duedate, o_returndate, o_usedays)values('$_POST[name]', '$_POST[delivery]', '$_POST[date]', '$_POST[returndate]', '$_POST[usedays]')");
		$order_id = mysql_insert_id();
		//create gatepass 
		$this->db->query("insert into gatepass(g_order)values('$order_id')");
		
		$orders = explode(",", $_POST['orders']);
		$total = 0;
		foreach($orders as $order){
			$order = explode(":", $order);
			$amount = $order[0];
			$type = $this->get_type($order[1]);
			$ppu = $this->get_ppu($order[1]);
			$total = $total + ($ppu*$amount);
			$this->db->query("insert into orders_variety(ov_oid, ov_type, ov_amount)values('$order_id', '$type', '$amount')");
		}
		$this->db->query("update orders set o_total='$total' where o_id='$order_id'");
		$data['result'] = "Order added successfully!";

		
		
		$recipient = $_POST['email'];	
		$subject = $this->get_option(2);	
		$body = $this->get_option(3);	
		mail($recipient, $subject, $body);
		
		$subject = $this->get_option(5);
		$body = $this->get_option(6);
		mail($this->get_option(1), $subject, $body);		

		if($_POST['source']=='1'){
		$data['title'] = "Order Added Successfully!";
		$this->load->view('header', $data);
		$this->load->view('order', $data);
		$this->load->view('footer', $data);
		}else{
		$data['title'] = "Order Added Successfully!";
		$this->load->view('header_admin', $data);
		$this->load->view('order_admin', $data);
		$this->load->view('footer', $data);
		}
	}
	public function process_order(){
		$this->db->query("insert into orders(o_name, o_address, o_email, o_mobile, o_delivery, o_duedate, o_returndate, o_usedays)values('$_POST[name]', '$_POST[address]', '$_POST[email]', '$_POST[mobile]', '$_POST[delivery]', '$_POST[date]', '$_POST[returndate]', '$_POST[usedays]')");
		$order_id = mysql_insert_id();
		//create gatepass 
		$this->db->query("insert into gatepass(g_order)values('$order_id')");
		//
		$orders = explode(",", $_POST['orders']);
		$total = 0;
		foreach($orders as $order){
			$order = explode(":", $order);
			$amount = $order[0];
			$type = $this->get_type($order[1]);
			$ppu = $this->get_ppu($order[1]);
			$total = $total + ($ppu*$amount);
			$this->db->query("insert into orders_variety(ov_oid, ov_type, ov_amount)values('$order_id', '$type', '$amount')");
		}
		$this->db->query("update orders set o_total='$total' where o_id='$order_id'");
		$data['result'] = "Order added successfully!";

		
		
		$recipient = $_POST['email'];	
		$subject = $this->get_option(2);	
		$body = $this->get_option(3);	
		mail($recipient, $subject, $body);
		
		$subject = $this->get_option(5);
		$body = $this->get_option(6);
		mail($this->get_option(1), $subject, $body);		

		if($_POST['source']=='1'){
		$data['title'] = "Order Added Successfully!";
		$this->load->view('header', $data);
		$this->load->view('order', $data);
		$this->load->view('footer', $data);
		}else{
		$data['title'] = "Order Added Successfully!";
		$this->load->view('header_admin', $data);
		$this->load->view('order_admin', $data);
		$this->load->view('footer', $data);
		}
	}
	public function get_type($type){
		$this->db->select("*");
		$this->db->from("types");
		$this->db->where("t_name", $type);
		$result = $this->db->get();
		$result = $result->result_array();
		return $result[0]['t_id'];
	}
	public function get_type_name($type){
		$this->db->select("*");
		$this->db->from("types");
		$this->db->where("t_id", $type);
		$result = $this->db->get();
		$result = $result->result_array();
		return $result[0]['t_name'];
	}
	public function get_ppu($type){
		$this->db->select("*");
		$this->db->from("types");
		$this->db->where("t_name", $type);
		$result = $this->db->get();
		$result = $result->result_array();
		return $result[0]['t_price'];
	}
public function get_customers(){
		$this->db->select("*");
		$this->db->from("customers");
		$result = $this->db->get();
		$result = $result->result_array();
		return $result;
	}
	public function get_orders(){
		$this->db->select("*");
		$this->db->from("orders");
		$result = $this->db->get();
		$result = $result->result_array();
		$newr = array();
		foreach($result as $r){
			$o_id = $r['o_id'];
			$this->db->select("orders_variety.*, types.*");
			$this->db->from("orders_variety");
			$this->db->join("types", "orders_variety.ov_type=types.t_id");
			$this->db->where("orders_variety.ov_oid", $o_id);
			$res = $this->db->get();
			$res = $res->result_array();
			$r['ov'] = $res;
			$newr[] = $r;
		}
		return $newr;
	}
	public function get_stock(){
		$this->db->select("*");
		$this->db->from("types");
		$result = $this->db->get();
		$result = $result->result_array();
		return $result;
	}
	public function get_purchases(){
		$this->db->select("purchases.*, types.*");
		$this->db->from("purchases");
		$this->db->join("types", "types.t_id=purchases.p_type");
		$result = $this->db->get();
		$result = $result->result_array();
		return $result;
	}
	public function get_order($id){
		$this->db->select("orders.*, gatepass.*");
		$this->db->from("orders");
		$this->db->join("gatepass", "orders.o_id=gatepass.g_order", "left");
		$this->db->where("orders.o_id", $id);
		$result = $this->db->get();
		$result = $result->result_array();
		$newr = array();
		foreach($result as $r){
			$o_id = $r['o_id'];
			$this->db->select("orders_variety.*, types.*");
			$this->db->from("orders_variety");
			$this->db->join("types", "orders_variety.ov_type=types.t_id");
			$this->db->where("orders_variety.ov_oid", $o_id);
			$res = $this->db->get();
			$res = $res->result_array();
			$r['ov'] = $res;
			$newr[] = $r;
		}
		return $newr[0];
	}
	public function update_gatepass(){
		/*print "<pre>";
		print_r($_POST);
		print "</pre>";*/
		$gatepass = $_POST['g_id'];
		$order = $_POST['o_id'];
		foreach($_POST as $key=>$value){
			if((substr($key, 0, 2)=="g_")&&($key!="g_id")){
				$this->db->query("update gatepass set $key='$value' where g_id='$gatepass'");
			}
			if((substr($key, 0, 2)=="o_")&&($key!="o_id")){
				$this->db->query("update orders set $key='$value' where o_id='$order'");
			}
			if(substr($key, 0, 2)=="ov"){
				$key = explode("-", $key);
				$ov_id = str_replace("ov", "", $key[0]);
				$key = $key[1];
				$this->db->query("update orders_variety set $key='$value' where ov_id='$ov_id'");
			}
		}
		redirect(base_url()."index.php/admin/order?id=".$order);
		
	}
	public function order(){
		//make payment
		if(isset($_POST['mode'])){
		$this->db->query("update orders set o_mode='$_POST[mode]', o_securitydeposit='$_POST[securitydeposit]', o_reciept='$_POST[reciept]', o_discount='$_POST[discount]', o_recieved='$_POST[total]', o_transportationcost='$_POST[transport]' where o_id='$_GET[id]'");
		}
		if(isset($_POST['vat'])){
		$this->db->query("update orders set o_vat='1' where o_id='$_GET[id]'");		
		}
		//change status
		if(isset($_GET['status'])){
			$this->db->query("update orders set o_status='$_GET[status]' where o_id='$_GET[id]'");
		}
		$data['order'] = $this->get_order($_GET['id']);
		
		$data['title'] = "Single Order";
		$this->load->view('header_admin', $data);
   		$this->load->view('single_order', $data);
   		$this->load->view('footer_admin', $data);
	}
	public function change_pass(){
			$data['title'] = 'Change Password';
						
			$this->load->view('header_admin.php',$data);
			$this->load->view('change_pass', $data);
			$this->load->view('footer', $data);
	}
	public function process_change_pass(){
			//check if pass is correct
			$old_pass = MD5($_POST['old']);
			$new_pass = MD5($_POST['new']);
			$sql = mysql_query("select * from users where id='$_POST[user]'");
			$row = mysql_fetch_array($sql);
			if($_POST['new']!=$_POST['confirm']){
				$data['result'] = "New password doesn't match confirmed password";
			}elseif($old_pass!=$row['password']){
				$data['result'] = "Wrong password!";
			}
			else{
				$this->db->query("update users set password='$new_pass' where id='$_POST[user]'");
				$data['result']= "Changed successfully!";
			}
			$data['title'] = 'Change Password';
		
			$this->load->view('header_admin.php',$data);
			$this->load->view('change_pass', $data);
			$this->load->view('footer', $data);
	}
	public function make_order(){
		$data['title'] = "Make Order";
		$data['types'] = $this->get_types();
		$this->load->view('header_admin', $data);
		$this->load->view('order_admin', $data);
		$this->load->view('footer_admin', $data);
	}
	public function get_types(){
		/*$this->db->select("*");
		$this->db->from("types");
		$result = $this->db->get();
		$result = $result->result_array();
		*/
		$result = $this->db->query("select * from types");
		$result = $result->result_array();
		return $result;
	}
public function customers_data(){
		
		$q = strtolower($_GET["q"]);
		if (!$q) return;
		
		$sql = "select * from customers where c_name LIKE '%$q%'";
		$rsd = mysql_query($sql);
		while($rs = mysql_fetch_array($rsd)) {
			$cname = $rs['c_name'];
			echo "$cname\n";
		}

	}

}

?>
