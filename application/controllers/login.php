<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

  function __construct()
  {
    parent::__construct();
  }

  function index()
  {$this->load->model('pages_m');
		$data['pages'] = $this->pages_m->get_pages();
    $data['title'] = "Login";
    $this->load->helper('form');
    $this->load->view('header', $data);
    $this->load->view('login_view', $data);
    $this->load->view('footer', $data);
  }

}

?>
