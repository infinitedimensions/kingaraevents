<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Order extends CI_Controller {
	public function index(){
		$this->load->model('pages_m');
		$data['pages'] = $this->pages_m->get_pages();
		
		$data['title'] = "Make Order";
		$data['types'] = $this->get_types();
		$this->load->view('header', $data);
		$this->load->view('order', $data);
		$this->load->view('footer', $data);
	}
	public function get_types(){
		/*$this->db->select("*");
		$this->db->from("types");
		$result = $this->db->get();
		$result = $result->result_array();
		*/
		$result = $this->db->query("select * from types");
		$result = $result->result_array();
		return $result;
	}
}
?>
