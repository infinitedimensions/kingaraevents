<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('pages_m');
	}
	public function index(){
		$data['pages'] = $this->pages_m->get_pages();
		$data['title'] = "Pages";
		$this->load->view("header_admin", $data);
		$this->load->view("show_pages", $data);
		$this->load->view("footer", $data);
	}
	public function view(){
			
		$id = $_GET['id'];
		$data['page'] = $this->pages_m->get_page($id);
$data['title'] = $data['page']['title'];
		$data['pages'] = $this->pages_m->get_pages(); 
		$this->load->view("header", $data);
		$this->load->view("show_page", $data);
		$this->load->view("footer", $data);
	}
	public function add(){
$data['title'] = "Add Page";
		$this->load->view("header_admin", $data);
		$this->load->view("add_page", $data);
		$this->load->view("footer", $data);
	}
	public function add_process(){
		$this->db->query("insert into pages(body, menu, title)values('$_POST[body]', '$_POST[menu]', '$_POST[title]')");
		redirect(base_url()."index.php/pages");
	}
	public function edit(){
		$id = $_GET['id'];
		$data['page'] = $this->pages_m->get_page($id);
		$data['title'] = "Edit Page";
		$this->load->view("header_admin", $data);
		$this->load->view("edit_page", $data);
		$this->load->view("footer", $data);
	}
	public function edit_process(){
		$id = $_POST['id'];
		$this->db->query("update pages set title='$_POST[title]', menu='$_POST[menu]', body='$_POST[body]' where id='$id'");
		redirect(base_url()."index.php/pages");
	}
	public function delete(){
		$id = $_GET['id'];
		$this->db->query("delete from pages where id='$id'");
		redirect(base_url()."index.php/pages");
	}
}	
?>
