<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data['title'] = "Home";
		$data['types'] = $this->get_types();
		$this->load->model('pages_m');
		$data['pages'] = $this->pages_m->get_pages();
		$data['menus'] = $this->get_menus();
		$this->load->view('header.php', $data);
		$this->load->view('welcome_message', $data);
		$this->load->view('footer.php', $data);
	}
	public function get_types(){
		$this->db->select("*");
		$this->db->from("types");
		$result = $this->db->get();
		$result = $result->result_array();
		return $result;
	}
	public function get_menus(){
		$this->db->select("*");
		$this->db->from("menus");
		$result = $this->db->get();
		$result = $result->result_array();
		$newresult = array();
		foreach($result as $r){
			$this->db->select("*");
			$this->db->from("menu_item");
			$this->db->where("mi_menu", $r['m_id']);
			$result2 = $this->db->get();
			$result2 = $result2->result_array();
				$newresult2 = array();
				foreach($result2 as $r2){
					$this->db->select("*");
					$this->db->from("menu_food");
					$this->db->where("mf_mi", $r2['mi_id']);
					$result3 = $this->db->get();
					$result3 = $result3->result_array();
					$r2['mf'] = $result3;
					$newresult2[] = $r2;					
				}
			$r['mi'] = $newresult2;
			$newresult[] = $r;
		}	
		return $newresult;
	}
	public function contact()
	{$this->load->model('pages_m');
		$data['pages'] = $this->pages_m->get_pages();
		$data['title'] = "Contact us";
		$this->load->view('header.php', $data);
		$this->load->view('contact', $data);
		$this->load->view('footer.php', $data);
	}
	public function process_contact(){
$this->load->model('pages_m');
		$data['pages'] = $this->pages_m->get_pages();
		$admin_email = $this->get_option(1);
		$body = "New message from ".$_POST['name']." of mobile number ".$_POST['number']." and email ".$_POST['email'].". ".$_POST['body'];
		mail($admin_email, $_POST['subject'], $body);

		$data['title'] = "Contact us";
		$data['result'] = "Message sent successfully!";
		$this->load->view('header.php', $data);
		$this->load->view('contact', $data);
		$this->load->view('footer.php', $data);
	}
	public function get_option($id){
$this->load->model('pages_m');
		$data['pages'] = $this->pages_m->get_pages();
		$this->db->select("*");
		$this->db->from("options");
		$this->db->where("opt_id", $id);
		$result = $this->db->get();
		$result = $result->result_array();
		return $result[0]['opt_value'];
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
