<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class pages_m extends CI_Model {
	public function get_pages(){
		$this->db->select('*');
		$this->db->from('pages');
		$result = $this->db->get();
		return $result->result_array();
	}
	public function get_page($id){
		$this->db->select('*');
		$this->db->from('pages');
		$this->db->where("id", $id);
		$result = $this->db->get();
		$result = $result->result_array();
		return $result[0];
	}
}
?>
