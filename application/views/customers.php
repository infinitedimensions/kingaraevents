<style type="text/css" title="currentStyle">
	@import "<?php echo base_url()?>assets/datatables/css/demo_page.css";
	@import "<?php echo base_url()?>assets/datatables/css/demo_table.css";
</style>
<script type="text/javascript" language="javascript" src="<?php echo base_url()?>assets/datatables/js/jquery.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url()?>assets/datatables/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$('#example').dataTable();
	} );
</script>
<div id="demo">
	<h3>Customers</h3>
<a href="<?php echo base_url();?>index.php/admin/add_customer">New Customer</a>
<table cellpadding="0" cellspacing="0" border="0" class="display" id="example" width="100%">
	<thead>
		<tr>
			<th># No.</th>
			<th>Company</th>
			<th>Contact Person</th>
			<th>Title</th>
			<th>Email</th>
			<th>Phone</th>
			<th>Address</th>
			<th>Physical Address</th>
			<th>Website</th>
		</tr>
	</thead>
	<tbody>
		<?php
			foreach($customers as $customer){
				print "<tr>";
				print "<td class='center'><a href='".base_url()."index.php/admin/customer?id=".$customer['c_id']."'>".$customer['c_id']."</a></td>";
				print "<td class='center'>".$customer['c_company']."</td>";
				print "<td class='center'>".$customer['c_name']."</td>";
				print "<td class='center'>".$customer['c_contactperson_title']."</td>";
				print "<td class='center'>".$customer['c_email']."</td>";
				print "<td class='center'>".$customer['c_phone']."</td>";
				print "<td class='center'>".$customer['c_pobox']."</td>";
				print "<td class='center'>".$customer['c_address']."</td>";
				print "<td class='center'>".$customer['c_website']."</td>";
				print "</tr>";
			}
		?>
		
	</tbody>
</table>
</div><div class="spacer" style="margin-bottom: 30px"></div>
