<style type="text/css" title="currentStyle">
	@import "<?php echo base_url()?>assets/datatables/css/demo_page.css";
	@import "<?php echo base_url()?>assets/datatables/css/demo_table.css";
</style>
<script type="text/javascript" language="javascript" src="<?php echo base_url()?>assets/datatables/js/jquery.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url()?>assets/datatables/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$('#example').dataTable();
	} );
</script>
<div id="demo">	<h3>Purchases</h3>
<table cellpadding="0" cellspacing="0" border="0" class="display" id="example" width="100%">
	<thead>
		<tr>
			<th>Purchase No.</th>
			<th>Name</th>
			<th>Total</th>
			<th>Date</th>
		</tr>
	</thead>
	<tbody>
		<?php
			foreach($purchases as $s){
				print "<tr>";
					print "<td class='center'>$s[p_id]</td>";
					print "<td class='center'>$s[t_name]</td>";
					print "<td class='center'>$s[p_amount]</td>";
					print "<td class='center'>$s[p_date]</td>";
				print "</tr>";
			}
		?>
		
	</tbody>
	<tfoot>
		<tr>
			<th>Purchase No.</th>
			<th>Name</th>
			<th>Total</th>
			<th>Date</th>
		</tr>
	</tfoot>
</table>
</div><div class="spacer" style="margin-bottom: 30px"></div>