<div class="bs-docs-example">
            <ul id="myTab" class="nav nav-tabs">
              <li class="active"><a href="#details" data-toggle="tab">Details</a></li>
              <li class=""><a href="#payment" data-toggle="tab">Payment</a></li>
              <li class=""><a href="#invoice" data-toggle="tab">Invoice</a></li>
              <li class=""><a href="#replacementinvoice" data-toggle="tab">Replacement Invoice</a></li>
 			  <li class=""><a href="#gatepass" data-toggle="tab">Gatepass</a></li>
 			   <li class=""><a href="#reciept" data-toggle="tab">Reciept</a></li>
            </ul>
              </li>
            </ul>
            <div id="myTabContent" class="tab-content">
              <div class="tab-pane fade active in" id="details">
                <table class="table table-striped">
<?php

	print "<tr><td>Customer</td><td class='center'>$order[o_name]</td>";
	print "</tr><tr><td>Due</td><td class='center'>$order[o_duedate]</td>";
	print "</tr><tr><td>Items</td><td class='center'>";
	//$order[o_variety]
	/*$c_vars = explode(',', $order['o_variety']);
		foreach($c_vars as $c_var){
				$cvars = explode(':', $c_var);
				print $cvars[1].':'.$cvars[2].':'.$cvars[0];
			print '<br />';
		}*/
		foreach($order['ov'] as $ov_){
			print $ov_['ov_amount'].' '.$ov_['t_name'];
			print "<br />";
		}
	print "</td>";
	print "</tr><tr><td>Status</td><td class='center'>".$order['o_status']."</td>";
print "</tr>";
if($order['o_vat']=='1'){
	print "<tr><td>VAT</td><td>Yes</td></tr>";
}else{
print "<tr><td>VAT</td><td>No</td></tr>";
}
	print "<tr><td>Total</td><td>".$order['o_total']."</td></tr>";
	print "<tr><td>Recieved</td><td>".$order['o_recieved']."</td></tr>";
	$due = $order['o_total']-$order['o_recieved'];
	print "<tr><td>Due</td><td>".$due."</td></tr>";
	
?>
<tr>
<td>
Change Status
</td>
<td>
	<form>
<select name="status" onchange="window.location.href='order?id=<?php echo $_GET['id']?>&status='+this.form.status.options[this.form.status.selectedIndex].value">
	<option></option>
	<option value="Pending">Pending</option>
	<option value="Delivered">Delivered</option>
	<option value="Returned">Returned</option>
	<option value="Cancelled">Cancelled</option>
</select>
</form>
</td>
</tr>
</table>
              </div>
              <div class="tab-pane fade" id="payment">
               <form action='' method='POST'>
<table class="table table-striped">
<?php
	print "<tr><td>Total Recieved</td><td><input type='text' name='total' value='".$order['o_recieved']."'></td></tr>";
	print "<tr><td>Mode of Payment</td><td><input type='text' name='mode' value='".$order['o_mode']."'></td></tr>";
	print "<tr><td>Transportation Cost</td><td><input type='text' name='transport' value='".$order['o_transportationcost']."'></td></tr>";
	print "<tr><td>Security Deposit</td><td><input type='text' name='securitydeposit' value='".$order['o_securitydeposit']."'></td></tr>";
	print "<tr><td>Receipt Number</td><td><input type='text' name='reciept' value='".$order['o_reciept']."'></td></tr>";
	if($this->session->userdata('logged_in')['is_admin']=="1"){
		print "<tr><td>Discount</td><td><input type='text' name='discount' value='".$order['o_discount']."'></td></tr>";
	}
	if($order['o_vat']=='1'){
	print "<tr><td><input type='checkbox' name='vat' value='".$order['o_vat']."' checked></td><td>Discount</td></tr>";
	}else{
	print "<tr><td><input type='checkbox' name='vat' value='".$order['o_vat']."'></td><td>VAT</td></tr>";
	}

	print "<tr><td></td><td><input type='submit' value='Submit' class='btn btn-primary'></td></tr>";
	
?>
</table>
</form>
              </div>
              <div class="tab-pane fade" id="invoice">
<iframe src="<?php echo base_url();?>index.php/admin/invoice?id=<?php echo $_GET['id']?>" width="99%" height="500px"></iframe>
              </div>
              <div class="tab-pane fade" id="replacementinvoice">
<iframe src="<?php echo base_url();?>index.php/admin/replacementinvoice?id=<?php echo $_GET['id']?>" width="99%" height="500px"></iframe>
              </div>
              <div class="tab-pane fade" id="reciept">
<iframe src="<?php echo base_url();?>index.php/admin/reciept?id=<?php echo $_GET['id']?>" width="99%" height="500px"></iframe>
              </div>
              <div class="tab-pane fade" id="gatepass">
              	<form action="<?php echo base_url();?>index.php/admin/update_gatepass" method="POST">
              		<input name="o_id" type="hidden" value="<?php echo $order['o_id'];?>">
              		<input name="g_id" type="hidden" value="<?php echo $order['g_id'];?>">
<h3>Gate Pass No: <?php echo $order['g_id'];?></h3>
<table class="table">
<tr>
	<td>P.O/L.S.O No</td><td><input name="g_pso" type="text" value="<?php echo $order['g_pso'];?>"></td>
	<td>DEL NO.</td><td><input name="g_delno" type="text" value="<?php echo $order['g_delno'];?>"></td>
	<td>Invoice No</td><td><?php echo $order['o_id'];?></td>
</tr>	
</table>
   
<h2 style="text-align:center">Kingara Gardens Limited</h2> 
<strong>Permission to remove company property</strong>
<br />
I Authorize <input name="g_authorizes" type="text" value="<?php echo $order['g_authorizes'];?>">
<br />
To remove the following items:
<table class="table table-striped">
<thead>
	<th>NO</th><th>Description</th><th>QTY Out</th><th>QTY In</th><th>Reason</th><th>Status</th><th>Remarks</th>
</thead>
<tbody>
<?php
$i = 1;
foreach($order['ov'] as $ov_){
		print "<tr>";
print "<td>".$i."</td><td>".$ov_['t_name']."</td><td>".$ov_['ov_amount']."</td><td><input name='ov".$ov_['ov_id']."-ov_gatepassin' value='".$ov_['ov_gatepassin']."' type='text'></td><td><input name='ov".$ov_['ov_id']."-ov_reason' type='text' value='".$ov_['ov_reason']."'></td><td><input name='ov".$ov_['ov_id']."-ov_status' type='text' value='".$ov_['ov_status']."'></td><td><input name='ov".$ov_['ov_id']."-ov_remarks' type='text' value='".$ov_['ov_remarks']."'></td>";
print "</tr>";		
$i++;
}
?>
</tbody>
</table> 
<table class="table table-striped">
<tbody>
<tr>
			<td>Date</td><td><input name="g_date" type="date" value="<?php echo $order['g_date'];?>"></td>
		
			<td>From</td><td><input name="g_from" type="text" value="<?php echo $order['g_from'];?>"></td>
		
			<td>To</td><td><input name="g_to" type="text" value="<?php echo $order['g_to'];?>"></td>
		</tr>
		<tr>
			<td>Expected date of return</td><td><input name="o_returndate" type="date" value="<?php echo $order['o_returndate'];?>"></td>
		
			<td>Actual date of return</td><td><input name="o_actualreturndate" type="date" value="<?php echo $order['o_actualreturndate'];?>"></td>
		
			<td>Usage days</td><td><input name="o_usedays" type="text" value="<?php echo $order['o_usedays'];?>"></td>
		</tr>
 
	<tr>
		<td>Authorized/Issued By</td><td><input name="g_authorizer" type="text"value="<?php echo $order['g_authorizer'];?>"></td><td>Date</td><td><input name="g_authorized_date" type="date" value="<?php echo $order['g_authorized_date'];?>"></td><td>Sign</td><td><input name="sign" type="text"></td>
	</tr>
	<tr>
		<td>Security</td><td><input name="g_security" type="text"value="<?php echo $order['g_security'];?>"></td><td>Date</td><td><input name="g_security_date" type="date" value="<?php echo $order['g_security_date'];?>"></td><td>Sign</td><td><input name="sign_security" type="text"></td>
	</tr>
	<tr>
		<td>Driver</td><td><input name="g_driver" type="text" value="<?php echo $order['g_driver'];?>"></td><td>ID No.</td><td><input name="g_driver_id" type="text" value="<?php echo $order['g_driver_id'];?>"></td><td>Sign</td><td><input name="sign_vehicle" type="text"></td>
	</tr>
	<tr>
		<td>Received By</td><td><input name="g_recieved_by" type="text" value="<?php echo $order['g_recieved_by'];?>"></td><td>Date</td><td><input name="g_recieved_date" type="date" value="<?php echo $order['g_recieved_date'];?>"></td><td>Sign</td><td><input name="sign_received" type="text"></td>
	</tr>
</tbody>
</table>     
<input type="submit" value="Submit" class="btn btn-primary">
</form>        
              </div>
            </div>
          </div>