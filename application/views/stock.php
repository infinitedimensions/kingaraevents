<style type="text/css" title="currentStyle">
	@import "<?php echo base_url()?>assets/datatables/css/demo_page.css";
	@import "<?php echo base_url()?>assets/datatables/css/demo_table.css";
</style>
<script type="text/javascript" language="javascript" src="<?php echo base_url()?>assets/datatables/js/jquery.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url()?>assets/datatables/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$('#example').dataTable();
	} );
</script>
<div id="demo">
	<a href="<?php echo base_url();?>index.php/admin/add_type">Add Items</a> | <a href="<?php echo base_url();?>index.php/admin/add_purchases">New Purchases</a> | <a href="<?php echo base_url();?>index.php/admin/purchases">Purchases</a> 
	<h3>Stock</h3>
<table cellpadding="0" cellspacing="0" border="0" class="display" id="example" width="100%">
	<thead>
		<tr>
			<th>Item No.</th>
			<th>Name</th>
			<th>Quantity</th>
			<th>In</th>
			<th>Out</th>
			<th>Price Per Unit</th>
			<th>Replacement Cost</th>
			<th>Purchase Price</th>
		</tr>
	</thead>
	<tbody>
		<?php
			foreach($stock as $s){
				print "<tr>";
					print "<td class='center'>$s[t_id]</td>";
					print "<td class='center'>$s[t_name]</td>";
					print "<td class='center'>$s[t_total]</td>";
					//back to non-oop
					$st = 0;
						$sql = mysql_query("select * from orders_variety where ov_type='$s[t_id]'");
						if(mysql_num_rows($sql)>0)
						{
							while($row = mysql_fetch_array($sql)){
								$order = $row['ov_oid'];
								$sql2 = mysql_query("select * from orders where o_id='$order'");
								$row2 = mysql_fetch_array($sql2);
								if($row2['o_status']=="Delivered"){
									$st = $st + $row['ov_amount'];
								}	
							}
						}
					//
					$in = $s['t_total']-$st;
					print "<td class='center'>".$in."</td>";
					print "<td class='center'>".$st."</td>";
					print "<td class='center'>$s[t_price]</td>";
					print "<td class='center'>$s[t_replacement_cost]</td>";
					print "<td class='center'>$s[t_purchase_price]</td>";
				print "</tr>";
			}
		?>
		
	</tbody>
	<tfoot>
		<tr>
			<th>Item No.</th>
			<th>Name</th>
			<th>Total</th>
			<th>In</th>
			<th>Out</th>
			<th>Price</th>
			<th>Replacement Cost</th>
			<th>Purchase Price</th>
		</tr>
	</tfoot>
</table>
</div><div class="spacer" style="margin-bottom: 30px"></div>